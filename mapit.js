/**
 * Created by dheerajchand on 3/7/16.
 */
/* The following is material to be used in helper functions*/

var styleCache = {}, // create styleCache variable
    defaultStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: [0, 0, 0, 1],
            width: 1
        }) // ,
        //fill: new ol.style.Fill({
        //    color: [255, 255, 255, 0]
        //})
    }), // default uggeaux style ['rgb(254,237,222)','rgb(253,190,133)','rgb(253,141,60)','rgb(230,85,13)','rgb(166,54,3)']
    color1 = 'rgb(254, 237, 222)',
    color2 = 'rgb(253, 190, 133)',
    color3 = 'rgb(253, 141, 60)',
    color4 = 'rgb(230, 85, 13)',
    color5 = 'rgb(166, 54, 3)',
    colorLevels = {
        0: color1,
        0.2: color2,
        0.4: color3,
        0.6: color4,
        0.8: color5,
    };     // map the classfication level codes to a colour value, grouping them

/* The following is helper functions that will be called by jQuery */

function computeMinMaxFromProperty(total_data, property, year) {
    var geometries = (total_data.objects[year].geometries),
        incidents = _(geometries).map(function (g) {
                if (property in g.properties) {
                    return g.properties[property];
                }
            })
            .filter()
            .value()
            .sort(function (a, b) {
                return a - b;
            });

    var minMax = {
        min: _(incidents).min(),
        max: _(incidents).max()
    };

    buildTable(geometries, property, year);
    return minMax;
} // close function definition for compute minMax

function buildStyle(feature, resolution, property, minMax) {

    var theMin = minMax['min'],
        theMax = minMax['max'],
        incidentsCount = feature.get(property);

    var classification = _(colorLevels).keys()
        .map(function (cl) {
            if (((incidentsCount - theMin) / (theMax - theMin)) >= Number(cl)) {
                return Number(cl);
            }
        })
        .max()


    if (!incidentsCount || !colorLevels[classification]) {

        //console.log(defaultStyle);
        return [defaultStyle];

    }

    // check styleCache

    if (typeof(styleCache[classification]) == 'undefined') {
        styleCache[classification] = new ol.style.Style({
            fill: new ol.style.Fill({
                color: colorLevels[classification]
            }),
            stroke: defaultStyle.stroke
        });
    }
    return [styleCache[classification]];


    //console.log('buildStyle', styleCache, classification)

}// close buildstyle

function buildLayer(map, property, minMax, filePath) {

    //console.log('buildLayer', map);

    var vectorLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: filePath,
            format: new ol.format.TopoJSON()
        }),
        style: function (feat, res) {
            //console.log('here');
            return buildStyle(feat, res, property, minMax);
        }
    })

    map.addLayer(vectorLayer);

} //close function definition for buildlayer

function buildTable(geometries, property, year) {

    var wardsAndIncidents = {};
    _(geometries).each(function (g) {
        var ward = g.properties['ward'],
            counts = g.properties[property];

        wardsAndIncidents[ward] = counts;

    });

    var filtered = _(wardsAndIncidents).omitBy(_.isUndefined).value();

    var titleString = _(property).capitalize() + " in Chicago " + "for " + year;

    // replace title

    document.getElementById('tabletitle').innerHTML = titleString;

    // delete rows

    ($('.computedresults').remove());

    // create new rows

    _(filtered)
        .forIn(function (counts, ward) {
            $("#datatable > tbody").append('<tr class="computedresults"><td>' + ward + '</td><td>' + counts + '</td></tr>');
        })
} // close buildTable

$(document).ready(function () {

    // var styleCache = {};
    var chicagoDefault = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: './topojsons/2015.topo.json',
            format: new ol.format.TopoJSON()
        }),
        style: defaultStyle
    });

    /* The following is OL3 code to build the map*/

    // create base layer

    var osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });


    // populate the map, centred on Chicago

    var map = new ol.Map({
        layers: [osmLayer],
        target: 'map',
        view: new ol.View({
            center: ol.proj.transform([-87.623177, 41.881832], 'EPSG:4326', 'EPSG:3857'),
            zoom: 10,
            minZoom: 10
        })
    });

    // now we are going to listen for changes to the radio buttons and build a layer accordingly

    $('#mapit').on('click', function (event) {

        var year = $("#year input:checked").val(),
            category = $("#category input:checked").val(),
            filePath = './topojsons/' + year + '.topo.json';

        $.getJSON(filePath)
            .then(function (data) {
                return computeMinMaxFromProperty(data, category, year);
            })
            .then(function (minMax) {
                buildLayer(map, category, minMax, filePath);
            })
    }); // close event listener

});